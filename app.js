"use strict"


const audiofiles = require("./audiofiles.js");
const server = require("./server.js");


const audioPath = "/media";


//search for audiofiles
audiofiles.searchPath(audioPath)
	.then( () => {

		//start server
		server.init();

		//re-scan once in a while
		setInterval(() => audiofiles.searchPath(audioPath), 30*1000);

	})
	.catch( (err) => {
		console.error("error starting soundboard");
		throw err;
	});
