"use strict";



const express = require("express");
const http = require("http");
const path = require("path");

const audiofiles = require("./audiofiles.js");


const port = 8888;
const wwwPath = path.join(__dirname, "www");


const app = express();
const server = http.createServer(app);
const io = require("socket.io").listen(server);


const self = {


	init: function(){

		//serve www folder
		app.use("/", express.static(wwwPath));

		//allow json payload (requires "content-type: application/json" header from client)
		app.use(express.json());

		//log traffic
		app.use( (req, res, next) => {
			console.log(req.method, req.originalUrl, req.body);
			next();
		});


		//very important
		app.get("/coffee", (req, res) => {
			res.status(418).send("I'm a teapot");
		});


		//register api endpoints
		app.get("/files", (req, res) => {
			res.send(JSON.stringify(audiofiles.getFiles()));
		});


		app.get("/file/:file", (req, res) => {

			const path = audiofiles.getPath(req.params.file);

			if(typeof path === "undefined"){
				return res.status(404).end();
			}

			res.sendFile(path);

		});


		app.post("/play", (req, res) => {

			//verify
			if(typeof req.body !== "object"){
				console.error("Invalid payload");
				res.status(500).send("Invalid payload");
				return;
			}

			res.send(req.body);
			io.emit("play", req.body.file);
/*
			try{

				//set roll
				if(typeof req.body.file !== "string"){
					console.error("Invalid file");
					res.status(500).send("Invalid file");
					return;
				}

				//play file
				audiofiles.play(req.body.file);

				//echo payload
				res.send(req.body);

			}catch(err){
				console.error("Error playing file", err);
				res.status(500).send(err.message);
			}
*/
		});	//post /play


		app.post("/broadcastPlay/:file", (req, res) => {
			
			if(typeof req.params.file === "undefined")	throw new Error("missing file parameter");

			res.send();
			io.emit("play", req.params.file);
		});


		//start server
		console.log("Server starting on *:" + port);
		return server.listen(port);

	},

	stop: function(callback){
		console.log("Stopping server");
		server.close(callback);
	}

}

module.exports = self;
