"use strict"

const fs = require('fs');
const path = require("path");
const player = require('play-sound')();


// key is filename, value full path
const cachedFiles = {};


/**
 * Determines whether the specified path is audio file.
 *
 * @param      {string}   path    The path
 * @return     {boolean}  True if the specified path is audio file, False otherwise.
 */
function isAudioFile(file){
	if(typeof file !== "string")	throw new Error("not a string");

	//test for .mp3 in path
	if( file.match(".mp3") ){
		return true;
	}else{
		return false;
	}

}


const self = {

	/**
	 * Get filenames as array
	 *
	 * @return     {Array}  The files.
	 */
	getFiles: function(){
		return Object.keys(cachedFiles);
	},


	/**
	 * Gets the absolute path of the given file.
	 *
	 * @param      {string}  key     The of the file
	 * @return     {string}  The path of the file.
	 */
	getPath: function(key){
		return cachedFiles[key];
	},


	/**
	 * Search given directory path for audiofiles.
	 *
	 * @param      {string}   dirpath  The path to be searched
	 * @return     {Promise}  Promise returning array of files found
	 */
	searchPath: function(dirpath){
		if(typeof dirpath !== "string")	throw new Error("invalid path");
	

		return new Promise( (resolve, reject) => {
		
			fs.readdir(dirpath, (err, files) => {
				if(err){
					console.error("error reading path:", dirpath);
					reject(err);
				}else{

					//loop files
					for(let file of files){

						const fullPath = path.join(dirpath, file);
						//console.log("found file", fullPath);

						if(isAudioFile(fullPath)){

							//add fiel to cache
							cachedFiles[file] = fullPath;

						}

					}

					//console.log("cached files:", cachedFiles);

					resolve(files);

				}
			});

		});

	},

	/**
	 * Play given audiofile.
	 *
	 * @param      {string}   file    The file to play
	 * @return     {Promise}  Promise resolving when file is done playing
	 */
	play: function(file){

		const audio = player.play(
			cachedFiles[file],
			{ timeout: 300 },
			function(err){
				if (err && !err.killed) throw (err);
			}
		);

	}

};


module.exports = self;
