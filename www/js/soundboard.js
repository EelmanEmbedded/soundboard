
const socket = io();

socket.on('reconnect', function(attemptNumber){
	location.reload();
});

socket.on('play', function(file){
	console.log("PLAY EVENT!!!!", file);
	playLocal(`${window.origin}/file/${file}`);
});


function play(filename){

	const payload = {
		file: filename
	};

	fetch(window.location.origin + "/play", {
		method: "post",
		headers: {
			"content-type": "application/json"
		},
		body: JSON.stringify(payload)
	})
	.catch( (err) => {
		console.error("error playing file", err);
	});

}


function createPlayButton(filename){

	const button = document.createElement("button");

	button.innerText = filename;
	button.setAttribute("fileToPlay", filename);

	//play remote
	button.onclick = function(){
		play(button.getAttribute("fileToPlay"));
	}

/*
	//play local
	button.onclick = function(){
		playLocal(`${window.origin}/file/${button.getAttribute("fileToPlay")}`);
	}
*/
	return button;
}

function createPlayButtons(files){
	
	console.log("creating buttons for", files);

	const buttonspace = document.getElementById("soundboard-buttons");

	for(let file of files){

		buttonspace.appendChild(
			createPlayButton(file)
		);

	}

}


function playLocal(url){
	new Audio(url).play();
}


window.onload = function(){

	//get files
	fetch(window.location.origin + "/files", {
		method: "get"
	})
	.then( (response) => response.json() )
	.then( (data) => createPlayButtons(data))
	.catch( (err) => {
		console.error("error loading files", err);
	});

};

